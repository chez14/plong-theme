## Plong

Plong are theme for Bootstrap 5. I just basically made a theme for my self so
that if i want somekind of branded nuance, i just need to install this pacakges
and call it a day. No more styled components, weird bug when reinventing the
wheels and etc.

So Plong means "devil" in "my" language.

## Usage

Because I haven't publish it to NPM, you will need to add GitLab's private
packagist for this.

- Add GitLab's private npm registry
    ```bash
    echo @chez14:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc

    ## or if you use yarn v1
    echo \"@chez14:registry\" \"https://gitlab.com/api/v4/packages/npm/\" >> .yarnrc
    ```
- Install the package
    ```bash
    npm i @chez14/plong

    # or if you use yarn:
    yarn add @chez14/plong
    ```
- Then import the `scss/_theme` file to your Bootstrap SASS
    ```sass
    @import "~bootstrap/scss/functions";

    // import them BEFORE variables.
    @import "~@chez14/plong/scss/theme";

    @import "~bootstrap/scss/variables";
    @import "~bootstrap/scss/maps";
    @import "~bootstrap/scss/mixins";
    @import "~bootstrap/scss/utilities";
    ```

- You're done.

## What did you change?

- Fonts \
  All fonts were embedded directly from Google Fonts. I might change this in the
  future to use my own CDN or embed it directly on this package. 
  - Headings: 
    - [Fredoka](https://fonts.google.com/specimen/Fredoka) by Hafontia \
      Used for latin and its diacritics. 
    - [Kosugi Maru](https://fonts.google.com/specimen/Kosugi+Maru) by MOTOYA \
      Used for Japanese and Chinese support (to some extent)
    - [Noto Color Emoji](https://fonts.google.com/noto/specimen/Noto+Color+Emoji) \
      Used for Emoji Support.
  - Body:
    - [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans), [Noto Sans JP](https://fonts.google.com/noto/specimen/Noto+Sans+JP) \
      Noto Sans family with added extra support for Japanese Characters.
    - [Noto Color Emoji](https://fonts.google.com/noto/specimen/Noto+Color+Emoji) \
      Used for Emoji Support.

- Colors \
  Colors are from my [branding](https://ch14.me/brand) guide. I mean, there's a
  reason why I mention "branding nuance" up above.
  - Primary
    - **Wandering White** \
      Pantone 427 C — `#d0d3d4` \
      This color are the main light color for this theme and my branding. \
      Used for:
      - Primary light
      - Foreground (text, etc)
      - `$light`

    - **Gloomy Gray** \
      Pantone 433 C — `#1d252d` \
      This color are the main dark color for this theme and my branding. \
      Used for:
      - Primary Dark
      - Background (body-bg, etc)
      - `$dark`

    - **Restless Red** \
      Pantone 1788 C — `#ee2737` \
      Main color for my branding. \
      Used for:
      - Primary color (`$primary`)


  - Complementary \
    Colors here are taken from analogous color harmony fomula.

    - **Optimistic Orange** \
      Pantone 1585 C — `#ff6a14` \
      First accent color from my main color. Obtained via tritone complementary
      color formula. \
      Used for:
      - Accent color A, `accent-a`
      - Originaly thought to use it for secondary, but I leave it for now.

    - **Paranormal Pink** \
      Pantone 225 C — `#df1995` \
      Second accent color from my main color. Obtained via tritone complementary
      color formula. **SHOULD RARELY BE USED**. \
      Used for:
      - Accent color B, `accent-b`
      - SHOULD RARELY BE USED.

  - Complementary Part 2 \
    Colors here are taken from tetradic color harmony fomula.

    - **Gladly Green** \
      Pantone 375 C — `#97d700` \
      The green-green thingy on your element. \
      Used for:
      - Success color

    - **Clinical Cyan** \
      Pantone 319 C — `#2cccd3` \
      The bluest of all blue, the cyanest of all cyan, thats why it obtained the
      title "clinical". \
      Used for:
      - Info color

    - **Persuasive Purple** \
      Pantone 266 C — `#753bbd` \
      The most persuasive element of color we have on the site. People SHOULD
      always look at this element when it present, hence the "persuasive". \
      Used for:
      - Danger color

Noticing there's no `warning` color? I keep using the Bootstrap 5's default
color for that part since I don't have anything to replace them.

## Resources

- CHEZ14 and Christianto's Branding: \
  https://ch14.me/brand

## License
This package are licensed as [MIT](./LICENSE). 

Feel free to use it, just make sure you don't use my name or brands over the
stuff you're making with. This are provided open for everyone to study or take
notes or comments to.