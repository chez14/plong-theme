const config = require('./webpack.config.common')

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const HotModuleReplacementPlugin = require('webpack').HotModuleReplacementPlugin;

module.exports = {
    ...config,
    devServer: {
        static: 'assets/lib/bootstrap',
        port: 8080,
        hot: true
    },
    plugins: [
        ...config.plugins
    ],
}
