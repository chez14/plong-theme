const fs = require('fs');
const path = require('path');

// Check if version argument is provided
if (process.argv.length < 3) {
  console.error('Usage: node update_version.js <version>');
  process.exit(1);
}

// Get version from argument and remove prefix 'v'
const version = process.argv[2].replace(/^v/, '');

// Read package.json
const packageJsonPath = path.join(process.cwd(), 'package.json');
let packageJson;
try {
  packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));
} catch (err) {
  console.error('Error reading package.json:', err);
  process.exit(1);
}

// Update version
packageJson.version = version;

// Write back to package.json
try {
  fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
  console.log(`Successfully updated project ${packageJson.name || 'unknown'} to version ${version}`);
} catch (err) {
  console.error('Error writing to package.json:', err);
  process.exit(1);
}
