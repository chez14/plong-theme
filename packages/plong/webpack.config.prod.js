// Common Webpack Configuration

const config = require('./webpack.config.common');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');

const isProduction = process.env.NODE_ENV !== 'development';

module.exports = {
    ...config,
    mode: 'production',
    devtool: 'hidden-source-map',
    plugins: [
        ...config.plugins
    ],

    // Ignore warnings from loaders from node_modules.
    ignoreWarnings: [
        {
            message: /\/node_modules\//,
            module: /(sass|scss|postcss)-loader/
        },
        (warning) => true
    ]
}
