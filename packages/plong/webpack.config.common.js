// Common Webpack Configuration

const path = require('path')
const fs = require('fs')
const glob = require('glob')

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const TerserPlugin = require('terser-webpack-plugin');

// Load the directories
const styleDir = path.join(__dirname, "src/scss/")
const scriptDir = path.join(__dirname, "src/js/")

let stylings = glob.sync('**', { cwd: styleDir, absolute: false }).reduce(function (obj, el) {
  let realpath = path.join(styleDir, el);
  let parsedPath = path.parse(el);
  // skips directory.
  let pathStats = fs.statSync(realpath);
  if (pathStats.isDirectory() || parsedPath.name.startsWith("_")) {
    return obj;
  }

  obj[path.join(parsedPath.dir, parsedPath.name, 'styles')] = realpath
  return obj
}, {});
let scripts = glob.sync('**', { cwd: scriptDir, absolute: false }).reduce(function (obj, el) {
  let realpath = path.join(scriptDir, el);
  let parsedPath = path.parse(el);
  // skips directory.
  let pathStats = fs.statSync(realpath);
  if (pathStats.isDirectory() || parsedPath.name.startsWith("_")) {
    return obj;
  }

  obj[path.join(parsedPath.dir, parsedPath.name, 'scripts')] = realpath
  return obj
}, {});

let entryPoints = { ...stylings, ...scripts };
// ===========

module.exports = {
  entry: {
    ...entryPoints
  },
  output: {
    path: path.resolve(__dirname, 'build/'),
    filename: '[name].js',
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  require('autoprefixer')
                ]
              }
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      }, {
        mimetype: 'image/svg+xml',
        scheme: 'data',
        type: 'asset/resource',
        generator: {
          filename: 'icons/[hash].svg'
        }
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[hash][ext]'
        }
      },
    ]
  },
  plugins: [
    new RemoveEmptyScriptsPlugin({ verbose: true }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      }),
    ],
    usedExports: true,
  },
  // for watchers!
  watchOptions: {
    ignored: ['**/node_modules'],
  },

  stats: {
    preset: 'normal',
    chunks: true,
    groupAssetsByChunk: true,
    groupAssetsByPath: true,
    errorsCount: true,
    warningsCount: true,
    publicPath: true,
  },

  // Ignore warnings from loaders from node_modules.
  ignoreWarnings: [
    {
      message: /\/node_modules\//,
      module: /(sass|scss|postcss)-loader/
    },
    (warning) => true
  ]
}
