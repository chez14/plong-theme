import { defineConfig } from 'astro/config';
import sitemap from "@astrojs/sitemap";
import react from "@astrojs/react";
import purgecss from "astro-purgecss";

// https://astro.build/config
export default defineConfig({
  integrations: [sitemap(), react(), purgecss()]
});
