import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { useCookies } from "react-cookie";
import styles from "./navbar.module.scss";


export interface ThemeNavbarProps {
    transparent?: boolean
}

export const COLOR_PREF = 'color-pref';

function ThemeNavbar({ transparent }: ThemeNavbarProps) {
    const [hideBg, setHideBg] = useState(true);

    const [cookies, setCookie, removeCookie] = useCookies([COLOR_PREF]);

    useEffect(() => {
        document.documentElement.setAttribute('data-bs-theme', cookies[COLOR_PREF]);
    }, [cookies])

    useEffect(() => {
        let listener = function () {
            setHideBg(window.scrollY <= 0);
        }

        window.addEventListener('scroll', listener);
        return () => {
            window.removeEventListener('scroll', listener);
        }
    }, [setHideBg])

    return <>
        <Navbar expand="lg" sticky='top' className={classNames(styles.navbar, { [styles.transparent]: transparent, [styles.hideBackground]: hideBg })}>
            <Container>
                    <Navbar.Brand  href="/"  className={styles.brand}>plong</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/controls">Controls</Nav.Link>
                    </Nav>
                    <Nav className="ms-auto">
                        <Nav.Link onClick={() => setCookie(COLOR_PREF, cookies[COLOR_PREF] === 'dark' ? 'light' : 'dark')}>
                            Dark/Light Mode
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </>
}

export default ThemeNavbar
