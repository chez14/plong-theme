import React from 'react'

function ImageFragment() {
    return (<>
        <div className="section">
            <svg className="img-fluid bd-placeholder-img bd-placeholder-img-lg"
                width="100%"
                height="250"
                xmlns="http://www.w3.org/2000/svg"
                role="img"
                aria-label="Placeholder: Responsive image"
                preserveAspectRatio="xMidYMid slice"
                focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#868e96" />
                <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Responsive image</text>
            </svg>
        </div>


        <div className="section">
            <svg className="img-thumbnail bd-placeholder-img"
                width="200"
                height="200"
                xmlns="http://www.w3.org/2000/svg"
                role="img"
                aria-label="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera: 200x200"
                preserveAspectRatio="xMidYMid slice"
                focusable="false">
                <title>A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera</title>
                <rect width="100%" height="100%" fill="#868e96" />
                <text x="50%" y="50%" fill="#dee2e6" dy=".3em">200x200</text>
            </svg>
        </div>
    </>
    )
}

export default ImageFragment
