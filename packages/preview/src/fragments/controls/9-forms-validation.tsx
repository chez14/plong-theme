import { FormCheck, FormControl, FormLabel, FormSelect } from 'react-bootstrap'

function FormsValidationFragment() {
    return (<>
        <div className="section">
            <form className="row g-3">
                <div className="col-md-4">
                    <FormLabel htmlFor="validationServer01" >First name</FormLabel>
                    <FormControl type="text" className="form-control is-valid" id="validationServer01" defaultValue="Mark" required />
                    <div className="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div className="col-md-4">
                    <FormLabel htmlFor="validationServer02" >Last name</FormLabel>
                    <FormControl type="text" className="form-control is-valid" id="validationServer02" defaultValue="Otto" required />
                    <div className="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div className="col-md-4">
                    <FormLabel htmlFor="validationServerUsername" >Username</FormLabel>
                    <div className="input-group has-validation">
                        <span className="input-group-text" id="inputGroupPrepend3">@</span>
                        <FormControl type="text" className="form-control is-invalid" id="validationServerUsername" aria-describedby="inputGroupPrepend3" required />
                        <div className="invalid-feedback">
                            Please choose a username.
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <FormLabel htmlFor="validationServer03" >City</FormLabel>
                    <FormControl type="text" className="form-control is-invalid" id="validationServer03" required />
                    <div className="invalid-feedback">
                        Please provide a valid city.
                    </div>
                </div>
                <div className="col-md-3">
                    <FormLabel htmlFor="validationServer04" >State</FormLabel>
                    <FormSelect className="is-invalid" id="validationServer04" required>
                        <option selected disabled>Choose...</option>
                        <option>...</option>
                    </FormSelect>
                    <div className="invalid-feedback">
                        Please select a valid state.
                    </div>
                </div>
                <div className="col-md-3">
                    <FormLabel htmlFor="validationServer05" >Zip</FormLabel>
                    <FormControl type="text" className="form-control is-invalid" id="validationServer05" required />
                    <div className="invalid-feedback">
                        Please provide a valid zip.
                    </div>
                </div>
                <div className="col-12">
                    <FormCheck
                        required
                        isInvalid
                        id="invalidCheck3"
                        label="Agree to terms and conditions"
                        feedback="You must agree before submitting."
                        feedbackType="invalid"
                    />
                </div>
                <div className="col-12">
                    <button className="btn btn-primary" type="submit">Submit form</button>
                </div>
            </form>
        </div>
    </>)
}

export default FormsValidationFragment
