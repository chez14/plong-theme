import React from 'react'
import { FormControl, FormSelect } from 'react-bootstrap'

function FormsSizingFragment() {
    return (<>
        <div className="section">
            <div className="mb-3">
                <FormControl className="form-control-lg" type="text" placeholder=".form-control-lg" aria-label=".form-control-lg example" />
            </div>
            <div className="mb-3">
                <FormSelect className="form-select-lg" aria-label=".form-select-lg example">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </FormSelect>
            </div>
            <div className="mb-3">
                <FormControl type="file" className="form-control-lg" aria-label="Large file input example" />
            </div>
        </div>


        <div className="section">
            <div className="mb-3">
                <FormControl className="form-control-sm" type="text" placeholder=".form-control-sm" aria-label=".form-control-sm example" />
            </div>
            <div className="mb-3">
                <FormSelect className="form-select-sm" aria-label=".form-select-sm example">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </FormSelect>
            </div>
            <div className="mb-3">
                <FormControl type="file" className="form-control-sm" aria-label="Small file input example" />
            </div>
        </div>
    </>)
}

export default FormsSizingFragment
