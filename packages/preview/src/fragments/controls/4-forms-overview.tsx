import { FormControl, FormLabel } from 'react-bootstrap'

function FormsOverviewFragment() {
    return (<>
        <div className="bd-example m-0 border-0">
            <form>
                <div className="mb-3">
                    <FormLabel htmlFor="exampleInputEmail1" className="form-label">Email address</FormLabel>
                    <FormControl type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div className="mb-3">
                    <FormLabel htmlFor="exampleInputPassword1" className="form-label">Password</FormLabel>
                    <FormControl type="password" className="form-control" id="exampleInputPassword1" />
                </div>
                <div className="mb-3">
                    <FormLabel htmlFor="exampleSelect" className="form-label">Select menu</FormLabel>
                    <select className="form-select" id="exampleSelect">
                        <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <div className="mb-3 form-check">
                    <FormControl type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <FormLabel className="form-check-label" htmlFor="exampleCheck1">Check me out</FormLabel>
                </div>
                <fieldset className="mb-3">
                    <legend>Radios buttons</legend>
                    <div className="form-check">
                        <FormControl type="radio" name="radios" className="form-check-input" id="exampleRadio1" />
                        <FormLabel className="form-check-label" htmlFor="exampleRadio1">Default radio</FormLabel>
                    </div>
                    <div className="mb-3 form-check">
                        <FormControl type="radio" name="radios" className="form-check-input" id="exampleRadio2" />
                        <FormLabel className="form-check-label" htmlFor="exampleRadio2">Another radio</FormLabel>
                    </div>
                </fieldset>
                <div className="mb-3">
                    <FormLabel className="form-label" htmlFor="customFile">Upload</FormLabel>
                    <FormControl type="file" className="form-control" id="customFile" />
                </div>
                <div className="mb-3 form-check form-switch">
                    <FormControl className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" defaultChecked />
                    <FormLabel className="form-check-label" htmlFor="flexSwitchCheckChecked">Checked switch checkbox input</FormLabel>
                </div>
                <div className="mb-3">
                    <FormLabel htmlFor="customRange3" className="form-label">Example range</FormLabel>
                    <FormControl type="range" className="form-range" min="0" max="5" step="0.5" id="customRange3" />
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>

    </>)
}

export default FormsOverviewFragment
