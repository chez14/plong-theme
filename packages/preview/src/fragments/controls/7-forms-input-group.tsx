import React from 'react'
import { FormControl, FormLabel, InputGroup } from 'react-bootstrap'

function FormsInputGroupFragment() {
    return (<>
        <div className="section">
            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                <FormControl type="text" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" />
            </InputGroup>
            <InputGroup className="mb-3">
                <FormControl type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                <InputGroup.Text id="basic-addon2">@example.com</InputGroup.Text>
            </InputGroup>
            <FormLabel for="basic-url" >Your vanity URL</FormLabel>
            <InputGroup className="mb-3">
                <InputGroup.Text id="basic-addon3">https://example.com/users/</InputGroup.Text>
                <FormControl type="text" id="basic-url" aria-describedby="basic-addon3" />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Text>$</InputGroup.Text>
                <FormControl type="text" aria-label="Amount (to the nearest dollar)" />
                <InputGroup.Text>.00</InputGroup.Text>
            </InputGroup>
            <InputGroup>
                <InputGroup.Text>With textarea</InputGroup.Text>
                <textarea className='form-control' aria-label="With textarea"></textarea>
            </InputGroup>
        </div>
    </>)
}

export default FormsInputGroupFragment
