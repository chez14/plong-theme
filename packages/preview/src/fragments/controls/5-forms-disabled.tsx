import React from 'react'
import { Button, FormControl, FormLabel } from 'react-bootstrap'

function FormsDisabledFragment() {
    return (<>
        <div className="section">
            <form>
                <fieldset disabled aria-label="Disabled fieldset example">
                    <div className="mb-3">
                        <FormLabel htmlFor="disabledTextInput" >Disabled input</FormLabel>
                        <FormControl type="text" id="disabledTextInput" placeholder="Disabled input" />
                    </div>
                    <div className="mb-3">
                        <FormLabel htmlFor="disabledSelect" >Disabled select menu</FormLabel>
                        <select id="disabledSelect" className="form-select">
                            <option>Disabled select</option>
                        </select>
                    </div>
                    <div className="mb-3">
                        <div className="form-check">
                            <FormControl className="form-check-input" type="checkbox" id="disabledFieldsetCheck" disabled />
                            <FormLabel className="form-check-label" htmlFor="disabledFieldsetCheck">
                                Can't check this
                            </FormLabel>
                        </div>
                    </div>
                    <fieldset className="mb-3">
                        <legend>Disabled radios buttons</legend>
                        <div className="form-check">
                            <FormControl type="radio" name="radios" className="form-check-input" id="disabledRadio1" disabled />
                            <FormLabel className="form-check-label" htmlFor="disabledRadio1">Disabled radio</FormLabel>
                        </div>
                        <div className="mb-3 form-check">
                            <FormControl type="radio" name="radios" className="form-check-input" id="disabledRadio2" disabled />
                            <FormLabel className="form-check-label" htmlFor="disabledRadio2">Another radio</FormLabel>
                        </div>
                    </fieldset>
                    <div className="mb-3">
                        <FormLabel htmlFor="disabledCustomFile">Upload</FormLabel>
                        <FormControl type="file" id="disabledCustomFile" disabled />
                    </div>
                    <div className="mb-3 form-check form-switch">
                        <FormControl className="form-check-input" type="checkbox" role="switch" id="disabledSwitchCheckChecked" defaultChecked disabled />
                        <FormLabel className="form-check-label" htmlFor="disabledSwitchCheckChecked">Disabled checked switch checkbox input</FormLabel>
                    </div>
                    <div className="mb-3">
                        <FormLabel htmlFor="disabledRange" >Disabled range</FormLabel>
                        <FormControl type="range" className="form-range" min="0" max="5" step="0.5" id="disabledRange" />
                    </div>
                    <Button type="submit" variant='primary'>Submit</Button>
                </fieldset>
            </form>
        </div>
    </>)
}

export default FormsDisabledFragment
