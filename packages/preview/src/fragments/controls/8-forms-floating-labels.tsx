import { FloatingLabel, FormControl } from 'react-bootstrap'

function FormsFloatingLabelsFragment() {
    return (<>
        <div className="section">
            <form>
                <FloatingLabel label="Email address" controlId="floatingInput" className='mb-3'>
                    <FormControl type="email" placeholder="name@example.com" />
                </FloatingLabel>
                <FloatingLabel label="Password" controlId="floatingPassword">
                    <FormControl type="password" placeholder="Password" />
                </FloatingLabel>
            </form>
        </div>
    </>)
}

export default FormsFloatingLabelsFragment
