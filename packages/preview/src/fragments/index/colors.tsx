import { Col, Row } from 'react-bootstrap'

import classNames from 'classnames'
import { When } from 'react-if'
import styles from "./colors.module.scss"

const colors = [
    { title: "Wandering White", color: 'wandering-white' },
    { title: "Gloomy Gray", color: 'gloomy-gray' },
    { title: "Restless Red", color: 'restless-red' },
    { title: "Optimistic Orange", color: 'optimistic-orange' },
    { title: "Paranormal Pink", color: 'paranormal-pink' },
    { title: "Gladly Green", color: 'gladly-green' },
    { title: "Clinical Cyan", color: 'clinical-cyan' },
    { title: "Persuasive Purple", color: 'persuasive-purple' },
]
const themeColors = [
    { title: "Primary", color: "primary" },
    { title: "Secondary", color: "secondary" },
    { title: "Success", color: "success" },
    { title: "Info", color: "info" },
    { title: "Warning", color: "warning" },
    { title: "Danger", color: "danger" },
    { title: "Light", color: "light" },
    { title: "Dark", color: "dark" },
    { title: "Accent A", color: "accent-a" },
    { title: "Accent B", color: "accent-b" },
];

const colorShades = [100, 200, 300, 400, 500, 600, 700, 800, 900];

function PalleteBoardItem({ title, colorVar, colorHex, className }: { title?: string, colorVar: string, colorHex: string, className: string }) {
    return <div className={classNames(styles.paletteBoard__el, { [styles.paletteBoard__el_header]: !!title }, className)}>
        <When condition={!!title}>
            <p className='fw-bold'>{title}</p>
        </When>
        <p className="font-monospace">${colorVar}</p>
        <p><code>{colorHex}</code></p>
    </div>
}


export function ColorsFragment() {
    return (
        <>
            <section className="my-5">
                <div className="mb-4">
                    <h2>Theme Colors</h2>
                </div>

                <Row>
                    {themeColors.map((color) => <Col md={4} lg={3} key={color.color}>
                        <div className={classNames(styles.paletteBoard, 'my-0')}>
                            <PalleteBoardItem title={color.title} colorVar={color.color} colorHex={styles[`${color.color}`]} className={styles[`palete-${color.color}`]} />
                        </div>
                    </Col>)}
                </Row >

            </section>
            <section className="my-5">
                <div className="mb-4">
                    <h2>Extra Colors</h2>
                    <p>These colors are added by <code>chez14</code>&apos;s to match his branding, the usual Bootstrap colors are still included if you use SASS/SCSS.</p>
                </div>
                <Row>
                    {colors.map((color) => <Col md={4} lg={3} key={color.color}>
                        <div className={styles.paletteBoard}>
                            <PalleteBoardItem title={color.title} colorVar={color.color} colorHex={styles[`${color.color}-500`]} className={styles[`palete-${color.color}`]} />

                            {colorShades.map((shade) => {
                                const paletteName = `${color.color}-${shade}`;
                                return <PalleteBoardItem
                                    key={paletteName}
                                    colorVar={paletteName}
                                    colorHex={styles[paletteName]}
                                    className={styles[`palete-${paletteName}`]}
                                />
                            })}
                        </div>
                    </Col>)}
                </Row >
            </section >
        </>
    )
}
